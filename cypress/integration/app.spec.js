describe('Button test', () => {  
  it('Check text button and click', () => {
      cy.visit('/')
      cy.get('button').should('contain', 'Sum')
      cy.findByRole('button', { name: /Sum/i }).click({ force: true })
    })
  });