import {Button, Card, Container, Col, Form, Row} from "react-bootstrap";
import sum from '../src/functions/sum';
import subtract from '../src/functions/subtract';

function App() {
  return (
    <Container fluid>
      <Row className="justify-content-md-center">
        <Col lg="4">
          <Card>
            <Card.Header>
              <Card.Title><h4>template-react</h4></Card.Title>
            </Card.Header>
            <Card.Body>
              <Form>
                <Form.Row>
                  <Col>
                  <Button onClick={() => {
                    alert(sum(2,3))
                  }}>
                    Sum
                  </Button>
                  </Col>
                  <Col>
                    <Button onClick={() => {
                      alert(subtract(8, 5))
                    }}>
                      Subtract
                    </Button>
                  </Col>
                </Form.Row>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
