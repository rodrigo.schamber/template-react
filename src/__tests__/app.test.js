import React from 'react';
import ReactDOM from 'react-dom';
import { render, screen } from '@testing-library/react';
import App from '../../src/App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

it('renders project name', () => {
  render(<App />);
  expect(screen.getByText('template-react')).toBeInTheDocument();
});

it('renders button text sum', () => {
  render(<App />);
  expect(screen.getByText('Sum')).toBeInTheDocument();
});

it('renders button text subtract', () => {
  render(<App />);
  expect(screen.getByText('Subtract')).toBeInTheDocument();
});